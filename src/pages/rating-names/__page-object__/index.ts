import type { Page, TestFixture } from '@playwright/test';
import { test } from '@playwright/test';

// Класс для реализации логики с страницы рейтинга котиков.

export class RatingPage {
    private page: Page
    public alertNotifierSelector: string
    public likesSelector: string
    public catLikesNumberElement: string

    constructor({
        page,
    }: {
        page: Page;
    }) {
        this.page = page;
        this.alertNotifierSelector = `//div[text()='Ошибка загрузки рейтинга']`;
        this.likesSelector = `//table[contains(@class, 'rating-names_table')][1]/tbody/tr/td[contains(@class, 'has-text-success')]`;
    }

    async openRatingPage() {
        return await test.step('Открыть страницу рейтинга котиков.', async () => {
            await this.page.goto('/rating')
        });
    }

    async getCatLikesNumberElement(row : Number) {
        return await test.step('Получить элемент с количеством лайков.', async () => {
            return await this.page.locator(`//table[contains(@class, 'rating-names_table')][1]/tbody/tr[${row}]/td[contains(@class, 'has-text-success')]`);
        });
    }
}

export type RatingPageFixture = TestFixture<
    RatingPage,
    {
        page: Page;
    }
    >;

export const ratingPageFixture: RatingPageFixture = async (
    { page },
    use
) => {
    const ratingPage = new RatingPage({ page });

    await use(ratingPage);
};