import { test as base, expect } from '@playwright/test';
import { RatingPage, ratingPageFixture} from '../__page-object__'

const test = base.extend<{ ratingPage: RatingPage }>({
  ratingPage: ratingPageFixture,
});

test('При ошибке сервера в методе rating - отображается попап ошибки', async ({ page, ratingPage }) => {
  /**
   * Замокать ответ метода получения рейтинга ошибкой на стороне сервера
   * Перейти на страницу рейтинга
   * Проверить, что отображается текст ошибка загрузки рейтинга
   */

  // Мок метода запроса рейтинга котиков. Мок прерывает запрос.
  await page.route(
    request => request.href.includes('cats/rating'),
    async route => {
      await route.abort();
    }
  );

  // Открыть страницу рейтинга.
  await ratingPage.openRatingPage();
  // Проверить, что отображается попап с ошибкой.
  await expect(page.locator(ratingPage.alertNotifierSelector)).toBeVisible();
});

test('Рейтинг котиков отображается', async ({ page, ratingPage }) => {
  /**
   * Перейти на страницу рейтинга
   * Проверить, что рейтинг количества лайков отображается по убыванию
   */

  await ratingPage.openRatingPage();
  
  const likesListRowsNumber = await page.locator(ratingPage.likesSelector).count();
  
  for (let row = 1; row <= likesListRowsNumber - 1; row += 2) {
    const currentElementValue = await (await ratingPage.getCatLikesNumberElement(row)).textContent();
    const nextElementValue = await (await ratingPage.getCatLikesNumberElement(row + 1)).textContent();
    expect(parseInt(currentElementValue)).toBeGreaterThanOrEqual(parseInt(nextElementValue));
  }
});